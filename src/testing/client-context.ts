import { createHttpAgentSendReceive } from "@oas3/http-send-receive";
import * as http from "http";
import { ClientBase, ClientOptions } from "../client.js";

export interface ClientContextOptions<Client extends ClientBase> {
    baseUrl: URL,
    clientType: { new(options: ClientOptions): Client }
}
export interface ClientContext<Client extends ClientBase> {
    client: Client
}
export async function withClientContext<Client extends ClientBase, T = void>(
    options: ClientContextOptions<Client>,
    job: (context: ClientContext<Client>) => Promise<T>,
) {
    const { baseUrl } = options;

    const agent = new http.Agent();
    const httpSendReceive = createHttpAgentSendReceive({ agent });
    try {
        const client = new options.clientType({ httpSendReceive, baseUrl });

        const result = await job({
            client,
        });

        return result;
    }
    finally {
        agent.destroy();
    }

}
