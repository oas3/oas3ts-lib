import test from "tape-promise/tape.js";
import { parseDeepObjectPropertyName, stringifyDeepObjectPropertyName } from "./deep-object.js";

test("deep-object", async t => {

    {
        const actual = parseDeepObjectPropertyName("a[b]", "a");
        const expected = "b";
        t.equal(actual, expected);
    }

    {
        const actual = parseDeepObjectPropertyName("a[b]", "b");
        const expected = undefined;
        t.equal(actual, expected);
    }

    {
        const actual = stringifyDeepObjectPropertyName("a", "b");
        const expected = "a[b]";
        t.equal(actual, expected);
    }

});

