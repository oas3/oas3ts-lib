import { Base64 } from "js-base64";

export function parseAuthorizationHeader(scheme: string, authorizationHeader: string | undefined) {
    if (authorizationHeader == null) return;

    const prefix = scheme + " ";

    if (authorizationHeader.toLowerCase().startsWith(prefix.toLowerCase())) {
        return authorizationHeader.substring(prefix.length);
    }

    return;
}

export function stringifyAuthorizationHeader(scheme: string, value: string) {
    const prefix = scheme + " ";

    return prefix + value;
}

export function parseBasicAuthorizationHeader(value: string | undefined) {
    const encoded = parseAuthorizationHeader("Basic", value);

    if (encoded == null) return;

    const decoded = Base64.decode(encoded);
    const [username, password] = decoded.split(":", 2);

    return { username, password };
}

export function stringifyBasicAuthorizationHeader(
    credential: { username: string, password: string },
) {
    const { username, password } = credential;

    const decoded = username + ":" + password;
    const encoded = Base64.encode(decoded);

    return stringifyAuthorizationHeader("Basic", encoded);
}

export function parseContentTypeHeader(value: string) {
    const parts = value.split(";");
    return parts[0];
}

export function parseAcceptHeader<T extends string>(headers: string[], types: Set<T>) {
    if (!Array.isArray(headers)) {
        headers = [headers];
    }

    const parsed = parse();
    const list = Array.from(parsed);
    list.sort(([, a], [, b]) => b - a);
    return list.map(([type]) => type);

    function* parse() {
        for (const header of headers) {
            const entries = header.split(/\s*,\s*/);

            for (const entry of entries) {
                const parts = entry.split(/\s*;\s*/);
                const type = parts.shift() as T | undefined;

                if (!type) continue;
                if (!types.has(type)) continue;

                const values: Record<string, string> = {};
                for (const part of parts) {
                    const pair = part.split(/\s*=\s*/, 2);
                    if (pair.length !== 2) continue;

                    const [key, value] = pair;
                    values[key] = value;
                }
                if ("q" in values) {
                    const q = parseFloat(values["q"]);

                    if (isNaN(q)) {
                        continue;
                    }

                    if (q <= 0) {
                        continue;
                    }

                    if (q > 1) {
                        yield [type, 1] as const;
                        continue;
                    }

                    yield [type, q] as const;
                    continue;
                }

                yield [type, 1] as const;
                continue;
            }
        }
    }
}

export function stringifyAcceptHeader<T extends string>(typeIterable: Iterable<T>) {
    const types = Array.from(typeIterable);

    return types.
        map((type, index) => `${type}; q=${1 - index / types.length}`).
        join(", ");
}
