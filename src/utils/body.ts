export function flushBody(
    body: (signal?: AbortSignal) => AsyncIterable<Uint8Array>,
) {
    const abortController = new AbortController();
    abortController.abort();

    job().catch(error => null);

    async function job() {
        for await (const chunk of body(abortController.signal)) {
            //
        }
    }
}
