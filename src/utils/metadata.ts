import { Metadata } from "../metadata.js";
import { Verb } from "./verb.js";

export function getOperationId(
    metadata: Metadata,
    path: string,
    verb: Verb,
) {
    return metadata.operationsMap[path]?.[verb];
}

export function getRouteVerbs(
    metadata: Metadata,
    path: string,
) {
    return Object.keys(metadata.operationsMap[path]) as Verb[];
}

export function getOperationRequestHeaders(
    metadata: Metadata,
    operationId: string,
) {
    return metadata.requestHeadersMap[operationId] ?? [];
}

export function getOperationResponseHeaders(
    metadata: Metadata,
    operationId: string,
) {
    return metadata.responseHeadersMap[operationId] ?? [];
}
