// see https://swagger.io/specification/#path-item-object
export type Verb =
    "GET" |
    "PUT" |
    "POST" |
    "DELETE" |
    "OPTIONS" |
    "HEAD" |
    "PATCH" |
    "TRACE";
