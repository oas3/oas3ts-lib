import { Promisable } from "type-fest";
import { DeserializationError, SerializationError } from "../errors/index.js";
import { formDecode, formEncode, Status } from "../utils/index.js";
import { deserializeTextValue } from "./text.js";

//#region interfaces

export type OutgoingFormRequestDefault<
    P extends object,
    T extends object,
    > = {
        readonly parameters: P;
    } & OutgoingFormContainer<T>

export type OutgoingFormRequest<
    P extends object,
    C extends string,
    T extends object,
    > = {
        readonly parameters: P;
        readonly contentType: C;
    } & OutgoingFormContainer<T>

export type OutgoingFormResponseDefault<
    S extends Status,
    P extends object,
    T extends object,
    > = {
        readonly status: S,
        readonly parameters: P,
    } & OutgoingFormContainer<T>

export type OutgoingFormResponse<
    S extends Status,
    P extends object,
    C extends string,
    T extends object,
    > = {
        readonly status: S,
        readonly parameters: P,
        readonly contentType: C;
    } & OutgoingFormContainer<T>

export type IncomingFormRequest<
    P extends object,
    C extends string,
    T extends object,
    > = {
        readonly parameters: P,
        readonly contentType: C;
    } & IncomingFormContainer<T>

export type IncomingFormResponse<
    S extends Status,
    P extends object,
    C extends string,
    T extends object,
    > = {
        readonly status: S;
        readonly parameters: P,
        readonly contentType: C;
    } & IncomingFormContainer<T>

//#endregion

//#region containers

export type OutgoingFormContainer<T extends object> =
    { stream(signal?: AbortSignal): AsyncIterable<Uint8Array> } |
    { entity(): Promisable<T> }

export type IncomingFormContainer<T extends object> =
    { stream(signal?: AbortSignal): AsyncIterable<Uint8Array> } &
    { entity(): Promisable<T> }

//#endregion

//#region serialization

export async function* serializeFormEntity<T extends object>(
    asyncEntity: Promisable<T>,
): AsyncIterable<Uint8Array> {
    const encoder = new TextEncoder();

    const entity = await asyncEntity;
    try {
        yield encoder.encode(formEncode<T>(entity));
    }
    catch {
        throw new SerializationError();
    }
}

export async function deserializeFormEntity<T extends object>(
    stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>,
): Promise<T> {
    const text = await deserializeTextValue(stream);

    try {
        const trimmed = text.trim();
        const entity = formDecode<T>(trimmed);

        return entity;
    }
    catch {
        throw new DeserializationError();
    }

}

//#endregion
