import { ClientBase, ClientMiddleware } from "../client.js";
import { flushBody, getParameterValue } from "../utils/index.js";

export interface RedirectMiddlewareOptions {
    onError?: (error: unknown) => void;
}

/**
 * transparent redirects!
 * 
 * Be aware that the client might send execute the stream (or similar, like entities or lines) factory more than once during a redirect when http 100 is not supported or enabled.
 * Also, permanent redirects are cached forever!
 * 
 * in other words handle with care :-)
 */
export function createRedirectMiddleware(
    options: RedirectMiddlewareOptions = {},
): ClientMiddleware {
    const cache = {} as Record<string, Record<string, string>>;

    return async function (this: ClientBase, request, next) {
        let location = cache[request.method]?.[request.url.href] ?? undefined;
        if (location) {
            const url = new URL(location, request.url);
            try {
                const response = await this.send({
                    ...request,
                    url,
                });
                return response;
            }
            catch (error) {
                delete cache[request.method][url.href];

                options.onError?.(error);
            }
        }

        const response = await next(request);
        const locationFromHeader = getParameterValue(response.headers, "location");
        if (locationFromHeader != null) {
            location = locationFromHeader;
        }

        /*
        no location header, so we cannot redirect
        */
        if (!location) return response;
        const url = new URL(location, request.url);

        switch (response.status) {
            case 301: // Moved Permanently
                if (request.method === "GET") {
                    cache[request.method] ??= {};
                    cache[request.method][request.url.href] = location;

                    flushBody(response.stream);

                    return this.send({
                        ...request,
                        url,
                    });
                }
                return response;

            case 302: // Found
                if (request.method === "GET") {
                    flushBody(response.stream);

                    return this.send({
                        ...request,
                        url,
                    });
                }
                return response;

            case 303: // See Other
                if (request.method === "GET") {
                    flushBody(response.stream);

                    return this.send({
                        ...request,
                        url,
                    });
                }
                return response;

            case 307: // Temporary Redirect
                flushBody(response.stream);

                return this.send({
                    ...request,
                    url,
                });

            case 308: // Permanent Redirect
                cache[request.method] ??= {};
                cache[request.method][request.url.href] = location;

                flushBody(response.stream);

                return this.send({
                    ...request,
                    url,
                });

            default: return response;
        }
    };
}
