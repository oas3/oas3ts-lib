import * as errors from "../errors/index.js";
import { ServerMiddleware } from "../server.js";

export function createErrorMiddleware(
    onError?: (error: unknown) => void,
): ServerMiddleware {
    return async function (route, request, next) {
        try {
            const response = await next(request);
            return response;
        }
        catch (error) {
            onError?.(error);

            if (
                error instanceof errors.IncomingRequestEntityDeserializationError ||
                error instanceof errors.IncomingRequestParameterExtractionError
            ) {
                return {
                    headers: {},
                    status: 400,
                };
            }

            if (error instanceof errors.AuthorizationFailedError) {
                return {
                    headers: {},
                    status: 401,
                };
            }

            if (error instanceof errors.NoRouteFoundError) {
                return {
                    headers: {},
                    status: 404,
                };
            }

            if (error instanceof errors.MethodNotAvailableError) {
                return {
                    headers: {},
                    status: 405,
                };
            }

            if (
                error instanceof errors.MissingRequestContentTypeError ||
                error instanceof errors.UnexpectedRequestContentType
            ) {
                return {
                    headers: {},
                    status: 415,
                };
            }

            if (
                error instanceof errors.IncomingRequestEntityValidationError ||
                error instanceof errors.IncomingRequestParametersValidationError
            ) {
                return {
                    headers: {},
                    status: 422,
                };
            }

            if (error instanceof errors.OperationNotRegisteredError) {
                return {
                    headers: {},
                    status: 501,
                };
            }

            return {
                headers: {},
                status: 500,
            };
        }

    };
}
