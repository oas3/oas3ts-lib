import yaml from "js-yaml";
import { ServerMiddleware } from "../server.js";

export interface SpecificationMiddlewareOptions {
    specification: object;
}

export function createSpecificationMiddleware(
    options: SpecificationMiddlewareOptions,
): ServerMiddleware {
    return async function (this, route, request, next) {
        if (!route) switch (request.path) {
            case "/openapi.json": {
                return {
                    headers: {
                        "content-type": "application/json",
                    },
                    status: 200,
                    async * stream() {
                        const encoder = new TextEncoder();
                        yield encoder.encode(JSON.stringify(options.specification));
                    },
                };
            }

            case "/openapi.yaml":
            case "/openapi.yml": {
                return {
                    headers: {
                        "content-type": "text/yaml",
                    },
                    status: 200,
                    async * stream() {
                        const encoder = new TextEncoder();
                        yield encoder.encode(yaml.dump(options.specification));
                    },
                };
            }

        }

        return next(request);
    };
}
